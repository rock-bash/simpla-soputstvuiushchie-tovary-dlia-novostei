<?PHP

require_once('api/Simpla.php');

class PostAdmin extends Simpla
{
	private	$allowed_image_extentions = array('png', 'gif', 'jpg', 'jpeg', 'ico');

	public function fetch()
	{
		$post = new stdClass;
		$related_products = array();
		if($this->request->method('post'))
		{
			$post->id = $this->request->post('id', 'integer');
			$post->name = $this->request->post('name');
			$post->date = date('Y-m-d', strtotime($this->request->post('date')));

			$post->visible = $this->request->post('visible', 'boolean');

			$post->url = trim($this->request->post('url', 'string'));
			$post->meta_title = $this->request->post('meta_title');
			$post->meta_keywords = $this->request->post('meta_keywords');
			$post->meta_description = $this->request->post('meta_description');

			$post->annotation = $this->request->post('annotation');
			$post->text = $this->request->post('body');

 			// Не допустить одинаковые URL разделов.
			if(($a = $this->blog->get_post($post->url)) && $a->id!=$post->id)
			{
				$this->design->assign('message_error', 'url_exists');
			}
			else
			{
				if(empty($post->id))
				{
					$post->id = $this->blog->add_post($post);
					$post = $this->blog->get_post(intval($post->id));
					$this->design->assign('message_success', 'added');
				}
				else
				{
					$this->blog->update_post($post->id, $post);
					$post = $this->blog->get_post(intval($post->id));
					$this->design->assign('message_success', 'updated');
				}
				// Удаление изображения
				if($this->request->post('delete_image'))
				{
					$this->blog->delete_image($post->id);
				}


  	    		// Загрузка изображения
				$image = $this->request->files('image');
				if(!empty($image['name']) && in_array(strtolower(pathinfo($image['name'], PATHINFO_EXTENSION)), $this->allowed_image_extentions))
				{
					$this->blog->delete_image($post->id);
					move_uploaded_file($image['tmp_name'], $this->root_dir.$this->config->original_images_dir.$image['name']);
					$this->blog->update_post($post->id, array('image'=>$image['name']));
				}

				// Связанные товары
				if(is_array($this->request->post('related_products')))
				{
					$rp = array();
					foreach($this->request->post('related_products') as $p)
					{
						$rp[$p] = new stdClass;
						$rp[$p]->product_id = $product->id;
						$rp[$p]->related_id = $p;
					}
					$related_products = $rp;
				}

				
				$query = $this->db->placehold('DELETE FROM __blog_related_products WHERE post_id=?', $post->id);
				$this->db->query($query);
				if(is_array($related_products))
				{
					$pos = 0;
					foreach($related_products  as $i=>$related_product)
						$this->blog->add_related_product($post->id, $related_product->related_id, $pos++);
				}
				// Связанные товары (The end)

				$post = $this->blog->get_post(intval($post->id));
			}
		}
		else
		{
			$post->id = $this->request->get('id', 'integer');
			$post = $this->blog->get_post(intval($post->id));
			
		}

		if(empty($post))
		{
			$post = new stdClass;
			$post->date = date($this->settings->date_format, time());
		}
		else
		{
			// Связанные товары
			$related_products =  $this->blog->get_related_products(intval($post->id));
			if(!empty($related_products))
			{
				foreach($related_products as &$r_p)
					$r_products[$r_p->related_id] = &$r_p;
				$temp_products = $this->products->get_products(array('id'=>array_keys($r_products)));
				foreach($temp_products as $temp_product)
					$r_products[$temp_product->id] = $temp_product;

				$related_products_images = $this->products->get_images(array('product_id'=>array_keys($r_products)));
				foreach($related_products_images as $image)
				{
					$r_products[$image->product_id]->images[] = $image;
				}
			}
			$this->design->assign('related_products', $related_products);
			// Связанные товары (The end)
		}

		$this->design->assign('post', $post);
		

		return $this->design->fetch('post.tpl');
	}
}
